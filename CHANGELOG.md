## Changelog [EN] 

### Version 1.0.0 - (Unreleased)

Initial stable release of ALASS.

#### Added
- Prepared for deployment and distribution of the application.

### Version 0.9.0 - (Unreleased)

Documented code and prepared for deployment.

#### Added
- Document the usage of your application, including installation instructions and usage examples. 
- Prepare your application for deployment, considering packaging, distribution, and installation procedures. 
- Publish documentation and releases for your project to make it accessible to others. 

### Version 0.8.0 - (Unreleased)

Added unit tests and integration tests to verify functionality.

#### Added
- Write unit tests to validate the correctness of individual functions and components. 
- Perform integration testing to ensure that different parts of the application work together as expected. 
- Conduct user testing to gather feedback and identify areas for improvement. 

### Version 0.7.0 - (Unreleased)

Enhanced user interaction with an intuitive interface.

#### Added
- Add functionality for users to interact with the application, such as inputting file paths or triggering analysis. 
- Provide options for users to view detailed logs or diagnostic information if needed.

### Version 0.6.0 - (Unreleased)

Implemented robust error handling and logging functionality.

#### Added
- Implement robust error handling to gracefully handle unexpected conditions or failures. 
- Set up logging to record important events and errors during application execution. 

### Version 0.5.0 - (Unreleased)

Ensured compatibility across different operating systems.

#### Added
- Ensure that your application compiles and runs correctly on Windows, Linux, and macOS. 
- Test the application on different operating systems to verify cross-platform compatibility. 

### Version 0.4.0 - (Unreleased)

Added a user interface to display identified issues and solutions.

#### Added
- Design a user interface to display identified issues and suggested solutions. 
- Present solutions in a clear and user-friendly format, including links to external resources if necessary. 

### Version 0.3.0 - (Unreleased)

Implemented fetching of solutions for identified issues.

#### Added
- Integrate with online platforms to search for solutions based on identified issues. 
- Implement web scraping or API calls to fetch relevant solutions for detected problems. 

### Version 0.2.0 - (Unreleased)

Added logic to scan parsed log entries for issues.

#### Added
- Implement logic to analyze log data and identify potential issues or errors.
- Define criteria for detecting errors or patterns indicating potential bugs or corruption.

### Version 0.1.0 - (Unreleased)

Initial implementation of file parsing functionality.

#### Added
- Implement file parsing functionality to read log files from the filesystem.
- Add logic to parse the contents of log files and extract relevant information.
- Utilize the `std::fs` module for file I/O operations.
- Utilize regular expressions for parsing log entries.

## Değişiklik Günlüğü [TR]

### Sürüm 1.0.0 - (Yayınlanmadı)

ALASS'ın ilk kararlı sürümü.

#### Eklendi
- Uygulamanın dağıtımı ve yayılması için hazırlandı.

### Sürüm 0.9.0 - (Yayınlanmadı)

Kod belgelendirildi ve dağıtım için hazırlandı.

#### Eklendi
- Uygulamanızın kullanımını belgeleyin, kurulum talimatları ve kullanım örnekleri içerir. 
- Uygulamanızı dağıtma, dağıtma ve kurulum prosedürlerini düşünerek hazırlayın. 
- Projeyi erişilebilir hale getirmek için belgelemeler ve sürümler yayınlayın. 

### Sürüm 0.8.0 - (Yayınlanmadı)

Fonksiyonelliği doğrulamak için birim testleri ve bütünleştirme testleri eklendi.

#### Eklendi
- İşlevlerin ve bileşenlerin doğruluğunu doğrulamak için birim testleri yazın. 
- Farklı uygulama parçalarının beklenen şekilde bir araya gelip gelmediğini doğrulamak için bütünleştirme testi yapın. 
- Geri bildirim toplamak ve iyileştirilecek alanları belirlemek için kullanıcı testi yapın.

### Sürüm 0.7.0 - (Yayınlanmadı)

Kullanıcı etkileşimini geliştirildi, sezgisel bir arayüz eklendi.

#### Eklendi
- Kullanıcıların uygulama ile etkileşim kurmasını sağlayacak işlevler eklendi, dosya yollarını girmek veya analiz başlatmak gibi.
- Kullanıcıların gerektiğinde ayrıntılı günlükleri veya teşhis bilgilerini görüntülemeleri için seçenekler sağlandı.

### Sürüm 0.6.0 - (Yayınlanmadı)

Sağlam hata işleme ve günlükleme işlevselliği eklendi.

#### Eklendi
- Beklenmedik koşulları veya başarısızlıkları nazikçe ele almak için sağlam hata işleme uygulandı. 
- Önemli olayları ve hataları uygulama çalışması sırasında kaydetmek için günlük oluşturuldu. 

### Sürüm 0.5.0 - (Yayınlanmadı)

Farklı işletim sistemlerinde uyumluluğu sağlandı.

#### Eklendi
- Uygulamanızın Windows, Linux ve macOS'ta doğru şekilde derlenip çalıştığından emin olun. 
- Uygulamayı farklı işletim sistemlerinde test ederek çapraz platform uyumluluğunu doğrulayın. 

### Sürüm 0.4.0 - (Yayınlanmadı)

Tanımlanan sorunları ve çözümleri kullanıcı arabirimine eklendi.

#### Eklendi
- Tanımlanan sorunları ve önerilen çözümleri kullanıcı arayüzünde görüntülemek için bir kullanıcı arabirimi tasarlandı. 
- Çözümleri açık ve kullanıcı dostu bir formatta sunun, gerektiğinde harici kaynaklara bağlantılar dahil edin. 

### Sürüm 0.3.0 - (Yayınlanmadı)

Tanımlanan sorunlar için çözümlerin getirilmesi eklendi.

#### Eklendi
- Tanımlanan sorunlara dayalı çözümleri aramak için çevrimiçi platformlarla entegre olun. 
- Algılanan sorunlar için ilgili çözümleri almak için web scraping veya API çağrıları uygulayın. 

### Sürüm 0.2.0 - (Yayınlanmadı)

Taranmış günlük girişleri için sorun kontrolü mantığı eklendi.

#### Eklendi
- Günlük verilerini analiz etmek ve potansiyel sorunları veya hataları belirlemek için mantık uygulandı.
- Olası hataları veya bozulma belirtileri gösteren kriterler tanımlandı.

### Sürüm 0.1.0 - (Yayınlanmadı)

Dosya ayrıştırma işlevselliğinin ilk uygulanması.

#### Eklendi
- Günlük dosyalarını dosya sisteminden okumak için dosya ayrıştırma işlevselliği uygulandı. 
- Günlük dosyalarının içeriğini ayrıştırmak ve ilgili bilgileri çıkarmak için mantık eklendi. 
- Dosya G/Ç işlemleri için `std::fs` modülünü kullandı. 
- Günlük girişlerini ayrıştırmak için düzenli ifadeler kullanıldı.
